#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: LGPL-3.0-or-later

;; r6rs-cbor is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-cbor is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

(import (rnrs)
        (srfi :64)
        (cbor))

(define (uint->bytevector int)
  (let* ((len (fxmax 1 (fxdiv (fxand -8 (fx+ 7 (bitwise-length int))) 8)))
         (ret (make-bytevector len)))
    (bytevector-uint-set! ret 0 int (endianness big) (bytevector-length ret))
    ret))

;; RFC 7049 Appendix A

(define integer-examples
  '((0                      #x00)
    (1                      #x01)
    (10                     #x0a)
    (23                     #x17)
    (24                     #x1818)
    (25                     #x1819)
    (100                    #x1864)
    (1000                   #x1903e8)
    (1000000                #x1a000f4240)
    (1000000000000          #x1b000000e8d4a51000)
    (18446744073709551615   #x1bffffffffffffffff)
    (18446744073709551616   #xc249010000000000000000)
    (-18446744073709551616  #x3bffffffffffffffff)
    (-18446744073709551617  #xc349010000000000000000)
    (-1                     #x20)
    (-10                    #x29)
    (-100                   #x3863)
    (-1000                  #x3903e7)))

(test-begin "decode-integer")
(for-each (lambda (x)
            (let ((expect (car x))
                  (p (open-bytevector-input-port (uint->bytevector (cadr x)))))
              (test-equal expect (cbor-read p))
              (test-assert (eof-object? (cbor-read p)))))
          integer-examples)
(test-end)

(define float-examples
  '((0.0s0                  #xf90000)
    (-0.0s0                 #xf98000)
    (1.0s0                  #xf93c00)
    (1.1d0                  #xfb3ff199999999999a)
    (1.5s0                  #xf93e00)
    (65504.0s0              #xf97bff)
    (100000.0f0             #xfa47c35000)
    (3.4028234663852886f+38 #xfa7f7fffff)
    (1.0d+300               #xfb7e37e43c8800759c)
    (5.960464477539063s-8   #xf90001)
    (0.00006103515625s0     #xf90400)
    (-4.0s0                 #xf9c400)
    (-4.1d0                 #xfbc010666666666666)
    ;; half:
    (+inf.0                 #xf97c00)
    (+nan.0                 #xf97e00)
    (-inf.0                 #xf9fc00)
    ;; single:
    (+inf.0                 #xfa7f800000)
    (+nan.0                 #xfa7fc00000)
    (-inf.0                 #xfaff800000)
    ;; double
    (+inf.0                 #xfb7ff0000000000000)
    (+nan.0                 #xfb7ff8000000000000)
    (-inf.0                 #xfbfff0000000000000)))

(define best-float-precision
  (do ((n 0 (+ n 1))
       (x 1.0 (/ x 2.0)))
    ((= 1.0 (+ 1.0 x)) n)))

(test-begin "decode-floats")
(for-each (lambda (x)
            (let ((expect (car x))
                  (p (open-bytevector-input-port (uint->bytevector (cadr x)))))
              (let ((v (cbor-read p)))
                (cond ((nan? expect)
                       (test-assert (nan? v)))
                      ((> best-float-precision 24)
                       (test-assert (= expect v)))
                      (else
                       (when (not (= expect v))
                         (display "(soft failure) expected: ")
                         (write expect)
                         (display " but got: ")
                         (write v)
                         (newline)))))
              (test-assert (eof-object? (cbor-read p)))))
          float-examples)
(test-end)

(define simple-examples
  '((#f         #xf4)
    (#t         #xf5)
    (null       #xf6)
    (undefined  #xf7)))
(test-begin "decode-simple")
(for-each (lambda (x)
            (let ((expect (car x))
                  (p (open-bytevector-input-port (uint->bytevector (cadr x)))))
              (test-equal expect (cbor-read p))
              (test-assert (eof-object? (cbor-read p)))))
          simple-examples)
(test-end)


(define misc-examples
  '(((simple 16)                     #xf0)
    ((simple 24)                     #xf818)
    ((simple 255)                    #xf8ff)

    ((tag 0 "2013-03-21T20:04:00Z")          #xc074323031332d30332d32315432303a30343a30305a)
    ((tag 1 1363896240)                      #xc11a514b67b0)
    ((tag 1 1363896240.5)                    #xc1fb41d452d9ec200000)
    ((tag 23 #vu8(#x01 #x02 #x03 #x04))      #xd74401020304)
    ((tag 24 #vu8(#x64 #x49 #x45 #x54 #x46)) #xd818456449455446)
    ((tag 32 "http://www.example.com")       #xd82076687474703a2f2f7777772e6578616d706c652e636f6d)

    (#vu8()                         #x40)
    (#vu8(#x01 #x02 #x03 #x04)      #x4401020304)

    (""                            #x60)
    ("a"                           #x6161)
    ("IETF"                        #x6449455446)
    ("\"\\"                        #x62225c)
    ("\x00fc;"                     #x62c3bc)
    ("\x6c34;"                     #x63e6b0b4)
    ("\x10151;"                    #x64f0908591)

    (#()                           #x80)
    (#(1 2 3)                      #x83010203)
    (#(1 #(2 3) #(4 5))            #x8301820203820405)
    (#(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25)
     #x98190102030405060708090a0b0c0d0e0f101112131415161718181819)

    ([]                            #xa0)
    ([(1 . 2) (3 . 4)]             #xa201020304)
    ([("a" . 1) ("b" . #(2 3))]    #xa26161016162820203)
    (#("a" [("b" . "c")])          #x826161a161626163)
    ([("a" . "A") ("b" . "B") ("c" . "C") ("d" . "D") ("e" . "E")]
     #xa56161614161626142616361436164614461656145)))

(test-begin "decode-misc")
(for-each (lambda (x)
            (let ((expect (car x))
                  (p (open-bytevector-input-port (uint->bytevector (cadr x)))))
              (test-equal expect (cbor-read p))
              (test-assert (eof-object? (cbor-read p)))))
          misc-examples)
(test-end)

(define indefinite-examples
  '((#vu8(#x01 #x02 #x03 #x04 #x05)         #x5f42010243030405ff)
    ("streaming"                            #x7f657374726561646d696e67ff)
    (#()                                    #x9fff)
    (#(1 #(2 3) #(4 5))                     #x9f018202039f0405ffff)
    (#(1 #(2 3) #(4 5))                     #x9f01820203820405ff)
    (#(1 #(2 3) #(4 5))                     #x83018202039f0405ff)
    (#(1 #(2 3) #(4 5))                     #x83019f0203ff820405)
    (#(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25)
     #x9f0102030405060708090a0b0c0d0e0f101112131415161718181819ff)
    ([("a" . 1) ("b" . #(2 3))]             #xbf61610161629f0203ffff)
    (#("a" [("b" . "c")])                   #x826161bf61626163ff)
    ([("Fun" . #t) ("Amt" . -2)]            #xbf6346756ef563416d7421ff)))

(test-begin "decode-indefinite")
(for-each (lambda (x)
            (let ((expect (car x))
                  (p (open-bytevector-input-port (uint->bytevector (cadr x)))))
              (test-equal expect (cbor-read p))
              (test-assert (eof-object? (cbor-read p)))))
          indefinite-examples)
(test-end)
