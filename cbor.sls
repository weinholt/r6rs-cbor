;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of r6rs-cbor, a CBOR library in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: LGPL-3.0-or-later

;; r6rs-cbor is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-cbor is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; CBOR

(library (cbor)
  (export
    cbor-error?
    cbor-error-reason
    cbor-null?
    cbor-undefined?
    ;; cbor-nesting-depth-limit
    ;; cbor-number-of-character-limit
    ;; cbor-generator
    ;; cbor-fold
    cbor-read
    ;; cbor-accumulator
    ;; cbor-write
    )
  (import
    (rnrs)
    (cbor half-float))

(define-condition-type &cbor-error &error
   make-cbor-error cbor-error?)

(define cbor-error-reason condition-message)

(define (cbor-null? obj) (eq? obj 'null))

(define (cbor-undefined? obj) (eq? obj 'undefined))

(define (cbor-error who message . irritants)
  (raise (condition
          (make-cbor-error)
          (make-who-condition who)
          (make-message-condition message)
          (make-irritants-condition irritants))))

(define (bytevector->uint* bv/int)
  (if (bytevector? bv/int)
      (if (zero? (bytevector-length bv/int))
          0
          (bytevector-uint-ref bv/int 0 (endianness big)
                               (bytevector-length bv/int)))
      bv/int))

(define (get-bytevector-n* port n who)
  (let ((bv (get-bytevector-n port n)))
    (unless (and (bytevector? bv) (fx=? (bytevector-length bv) n))
      (cbor-error who "Early end of data" n who))
    bv))

(define (get-u8* port who)
  (let ((b (get-u8 port)))
    (unless (fixnum? b)
      (cbor-error who "Early end of data" port who))
    b))

(define (get-value port additional-info who)
  (if (fx<? additional-info 24)
      additional-info
      (case additional-info
        ((24) (get-bytevector-n* port 1 who))
        ((25) (get-bytevector-n* port 2 who))
        ((26) (get-bytevector-n* port 4 who))
        ((27) (get-bytevector-n* port 8 who))
        ((28 29 30) (cbor-error who "Invalid additional-info" additional-info))
        (else (cbor-error who "Internal error: indefinite value" additional-info)))))

(define (get-indefinite port major-type who)
  (case major-type
    ((2)
     (call-with-bytevector-output-port
       (lambda (p)
         (let lp ()
           (let ((v (get-tag port who)))
             (cond ((eq? v 'break))
                   ((not (bytevector? v))
                    (cbor-error who "Invalid value in indefinite-length bytevector"))
                   (else
                    (put-bytevector p v)
                    (lp))))))))
    ((3)
     (call-with-string-output-port
       (lambda (p)
         (let lp ()
           (let ((v (get-tag port who)))
             (cond ((eq? v 'break))
                   ((not (string? v))
                    (cbor-error who "Invalid value in indefinite-length string"))
                   (else
                    (put-string p v)
                    (lp))))))))
    ((4)
     (let lp ((acc '()))
       (let ((v (get-tag port who)))
         (if (eq? v 'break)
             (list->vector (reverse acc))
             (lp (cons v acc))))))
    ((5)
     (let lp ((acc '()))
       (let ((k (get-tag port who)))
         (if (eq? k 'break)
             (reverse acc)
             (let ((v (get-tag port who)))
               (lp (cons (cons k v) acc)))))))
    ((7) 'break)
    (else
     (cbor-error who "Invalid indefinite-length value" major-type))))

(define (get-tag port who)
  (let* ((b (get-u8* port who))
         (major-type (fxbit-field b 5 8))
         (additional-info (fxbit-field b 0 5)))
   (if (eqv? additional-info 31)
       (get-indefinite port major-type who)
       (case major-type
         ((0) (bytevector->uint* (get-value port additional-info who)))
         ((1) (bitwise-not (bytevector->uint* (get-value port additional-info who))))
         ((2) (let ((len (get-value port additional-info who)))
                (get-bytevector-n* port len who)))
         ((3) (let ((len (get-value port additional-info who)))
                (utf8->string (get-bytevector-n* port len who))))
         ((4)
          (let ((len (bytevector->uint* (get-value port additional-info who))))
            (do ((ret (make-vector len #f))
                 (i 0 (fx+ i 1)))
                ((fx=? i len) ret)
              (vector-set! ret i (get-tag port who)))))
         ((5)
          (do ((len (bytevector->uint* (get-value port additional-info who)))
               (i 0 (fx+ i 1))
               (ret '() (let* ((k (get-tag port who))
                               (v (get-tag port who)))
                          (cons (cons k v) ret))))
              ((fx=? i len)
               (reverse ret))))
         ((6)
          ;; Tagged item
          (let* ((tag (bytevector->uint* (get-value port additional-info who)))
                 (embedded (get-tag port who)))
            (case tag
              ((2)
               (unless (bytevector? embedded)
                 (cbor-error who "Invalid positive bignum" embedded))
               (bytevector->uint* embedded))
              ((3)
               (unless (bytevector? embedded)
                 (cbor-error who "Invalid native bignum" embedded))
               (bitwise-not (bytevector->uint* embedded)))
              ((55799) embedded)
              (else
               ;; TODO: let the user supply tag decoders?
               (list 'tag tag embedded)))))
         (else
          (let ((value (get-value port additional-info who)))
            (case additional-info
              ((20) #f)
              ((21) #t)
              ((22) 'null)
              ((23) 'undefined)
              ((25) (bytevector-ieee-half-ref value 0 (endianness big)))
              ((26) (bytevector-ieee-single-ref value 0 (endianness big)))
              ((27) (bytevector-ieee-double-ref value 0 (endianness big)))
              (else
               (list 'simple (bytevector->uint* value))))))))))

(define (cbor-read port/gen)
  (define who 'cbor-read)
  (when (procedure? port/gen)
    (error 'cbor-read "TODO: generators"))
  (if (port-eof? port/gen)
      (eof-object)
      (let ((v (get-tag port/gen who)))
        (when (eq? v 'break)
          (cbor-error who "Invalid break"))
        v))))

