# (cbor)

This is r6rs-cbor, an implementation of the CBOR binary interchange
format in R6RS Scheme. It a binary JSON-alike format used in IoT
protocols.

## Limitations

The whole thing is a work in progress. Right now only a decoder is
provided.

## Standards

This package does not claim to fully conform to every requirement in
these documents, but the goal is to support all of them. Any *MUST*
requirement that is not implemented is considered to be a bug.

- RFC 7049 Concise Binary Object Representation (CBOR). C. Bormann, P. Hoffman.
           October 2013. (DOI: 10.17487/RFC7049)
- RFC 8742 Concise Binary Object Representation (CBOR) Sequences. C. Bormann.
           February 2020. (Status: PROPOSED STANDARD) (DOI: 10.17487/RFC8742)
- RFC 8746 Concise Binary Object Representation (CBOR) Tags for Typed Arrays.
           C. Bormann, Ed.. February 2020. (DOI: 10.17487/RFC8746)

## API

The API will be (if possible) modelled on the JSON API
in [SRFI-180][srfi-180], with adjustments made for CBOR.

 [srfi-180]: https://srfi.schemers.org/srfi-180/srfi-180.html

## Installation

Use [Akku.scm](https://akkuscm.org) and install into your project with
`akku install r6rs-cbor`.

## Portability

## License

r6rs-cbor is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

r6rs-cbor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
