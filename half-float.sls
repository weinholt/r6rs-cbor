;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of r6rs-cbor, a CBOR library in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: LGPL-3.0-or-later

;; r6rs-cbor is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; r6rs-cbor is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; IEEE half-precision

;; The decoding algorithm is from RFC 7049. It's better to use native
;; instructions instead (e.g. F16C).

(library (cbor half-float)
  (export
    bytevector-ieee-half-native-ref
    bytevector-ieee-half-native-set!
    bytevector-ieee-half-ref
    bytevector-ieee-half-set!)
  (import
    (rnrs))

(define (bytevector-ieee-half-native-ref bv idx)
  (bytevector-ieee-half-ref bv idx (native-endianness)))

(define (bytevector-ieee-half-native-set! bv idx v)
  (bytevector-ieee-half-set! bv idx v (native-endianness)))

(define (decode-single s)
  (let ((tmp (make-bytevector 4)))
    (bytevector-u32-native-set! tmp 0 s)
    (bytevector-ieee-single-native-ref tmp 0)))

(define (bytevector-ieee-half-ref bv idx endian)
  (let* ((h (bytevector-u16-ref bv idx endian))
         (v (bitwise-ior (bitwise-arithmetic-shift-left (fxand h #x7fff) 13)
                         (bitwise-arithmetic-shift-left (fxand h #x8000) 16))))
    (if (not (eqv? (fxand h #x7c00) #x7c00))
        (* (decode-single v) (expt 2.0 112))
        (decode-single (bitwise-ior v #x7f800000)))))

(define (bytevector-ieee-half-set! bv idx v endian)
  (error 'bytevector-ieee-half-set! "Not implemented" bv idx v endian)))